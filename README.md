# Face Recognition Using PCA Based Eigenfaces. #

Face recognition can be applied for a wide variety of problems like image and film processing, human-computer interaction, criminal identification etc.
Our goal is to implement the model for a particular face and distinguish it from a large number of stored faces with some real-time variations as well. 


### Pre requisites ###

* Matlab R2015a or higher.
* AT&T Labs database.

### Developing ###

* Run Mio.m to start the project.

### Screen Shots ###

![1.jpg](https://bitbucket.org/repo/7Lpk8g/images/280361006-1.jpg)
![1.jpg](https://bitbucket.org/repo/7Lpk8g/images/467888824-1.jpg)
![1.jpg](https://bitbucket.org/repo/7Lpk8g/images/2161059079-1.jpg)
![1.jpg](https://bitbucket.org/repo/7Lpk8g/images/703302443-1.jpg)
![1.jpg](https://bitbucket.org/repo/7Lpk8g/images/2900727623-1.jpg)

### Restlts ###

![2.jpg](https://bitbucket.org/repo/7Lpk8g/images/785437943-2.jpg)
![2.jpg](https://bitbucket.org/repo/7Lpk8g/images/497529552-2.jpg)

### Who do I talk to? ###

* Mayank Shekhar
* E-mail- mayankshekhar@outlook.in
* **Other team members.**
* Sourav Gupta
* Dipanjan Das
* Hitesh Tikmani
* Manish Chakraborty